/*
libraries.h

Chabby 1.02 Beta
================

Latest changes done by Peter Cizmar (13.7.2016)
*/

#ifndef LIBRARIES_H
#define LIBRARIES_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <winsock2.h>
#include <windows.h>

#pragma comment(lib, "ws2_32.lib") //Winsock Library

#endif