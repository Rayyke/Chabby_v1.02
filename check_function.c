/*
check_functions.c

Chabby 1.02 Beta
================

Latest changes done by Peter Cizmar (5.10.2016)
*/

#include "macros.h"
#include "libraries.h"
#include "main.h"
#include "check_functions.h"
#include "commands.h"

int test_password(char password_to_test[], char password[])
{
	int i;

	for (i = 0; i < strlen(password); i++)
	{
		if (password_to_test[i] != password[i]) 
		{
			return 1;
		}
	}

	return 0;
}

int valid_id (char id_string[])
{
	if (id_string[1] == ' ' || id_string[1] == '\0')
	{
		if (id_string[0] >= '0' && id_string[0] <= '9')
		{
			return 0;
		}
	}
	else
	{
		if ((id_string[0] >= '0' && id_string[0] <= '9') && (id_string[1] >= '0' && id_string[1] <= '9'))
		{
			return 0;
		}
	}

	return 1;
}

int only_numbers (char string[]) 
{
	register int i;

	for (i = 0; i < (int)strlen(string); i++) 
	{
		if (string[i] < '0' || string[i] > '9')
		{
			return 1;
		}
	}

	return 0;
}

int online_users (SOCKET *client_socket, int max_clients) 
{
	register int i;
	int counter = 0;

	for (i = 0; i < max_clients; i++) 
	{
		if (client_socket[i] != 0)
		{
			counter++;
		}	
	}

	return counter;
}

int online_admins (int admins[], int max_clients) 
{
	register int i;
	int counter = 0;

	for (i = 0; i < max_clients; i++) 
	{
		if (admins[i] == 1) 
		{
			counter++;
		}		
	}

	return counter;
}