New note: This is really old project. Most likely NO updates will be available.

---------------------------------------------------------------------
   Welcome to Chabby 1.02 Beta, open source chat lobby program! :)  
---------------------------------------------------------------------
   Author: Peter Čižmár						    
---------------------------------------------------------------------

> Change log

	1.0 --> first version
	1.01 --> added '-status' and '-users' , fixed -rename (incorrect display)
	1.02 --> fixed the missing characters, which were caused by the delay, server now accepts not only character by character, but the whole string


> How to use it

1) Run program, it basically runs the server, enter the required values, such as port, max clients and admin password
2) Clients connect only via windows console - terminal
	a) press: windows + R
	b) type: cmd 
	c) press: OK
	d) most likely, terminal is disabled, so enter: pkgmgr /iu:"TelnetClient"
	e) hit enter, it will take up to maybe couple of minutes
	f) try to type: telnet and hit enter - if it says it is not recognizable, u didn´t entered to command from "d)" point correctly, try it again
					     - if it does nothing, it´s okay, just wait couple more seconds, maybe try to restart console or so
					     - if it says something like welcome to telnet, BINGO!
	g) server runs on some IP address, you need to know that address
	h) server master can type: ipconfig/all to the windows command to get his IP, there is something like (preferred) after the correct IP number
	i) also server master entered port, you need to know that number also, it looks like "8888"
	j) now only type to your windows console: telnet IP port (example: telnet 192.168.0.103 8888)
	k) you are connected!
3) Recommended height of the window is 50+ lines and recommended width is 80+ symbols.
	

> During the chat, you can use couple of commands, here they are (note: "-" thingy is important there, it indicates possible command)

	-pm [ID] [message] --> send a personal message to user
	-mute [ID] --> mute user
	-unmute [ID] --> unmute user
	-muteall --> simply mute all tards
	-unmuteall --> simply unmute all tards
	-rename [new name] --> change your name from [User xx] to [New name | ID: xx]
	-admin [password] --> makes you and admin, you can kick and silence
	-kick [ID] --> kick user, requires admin privilages
	-silence [ID] --> block the output communication of user (including PMs), requires admin privilages
	-unsilence [ID] --> allow the output communication of user (including PMs), requires admin privilages
	-status --> show the up-time of the server, number of online users and number of admins
	-users --> show list of online users


> As far as I mentioned, program is open source, so feel free to implement parts of the code to you projects :)
