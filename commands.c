/*
commands.c

Chabby 1.02 Beta
================

Latest changes done by Peter Cizmar (16.10.2016)
*/

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS

#include "macros.h"
#include "libraries.h"
#include "main.h"
#include "check_functions.h"
#include "commands.h"

void command_pm (SOCKET client_socket[], int current_client, char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE], 
					int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS], char current_client_name_string[], 
					char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH])
{	
	char sending_string[MAX_NAME_LENGTH + 34];
	int i;
	int id = 0;
	char id_string[2] = { 0 };
	
	id_string[0] = messages[current_client][4];
	id_string[1] = messages[current_client][5];

	if (valid_id(id_string) == 0)
	{
		id = strtol(id_string, NULL, 10);

		if (mute_id[id][current_client] == 0 && client_socket[id] != 0 && id != current_client)
		{
			if (messages[current_client][5] == ' ')
			{
				for (i = 6; i < MAX_RECEIVE; i++)
				{
					messages[current_client][i - 6] = messages[current_client][i];
				}
			}
			else
			{
				for (i = 7; i < MAX_RECEIVE; i++)
				{
					messages[current_client][i - 7] = messages[current_client][i];
				}
			}

			strcat(messages[current_client], "\n\r");

			send(client_socket[id], current_client_name_string, (int)strlen(current_client_name_string), 0);
			send(client_socket[id], "<PM> ", 5, 0);
			send(client_socket[id], messages[current_client], (int)strlen(messages[current_client]), 0);

			for (i = 0; i < MAX_RECEIVE; i++)
			{
				messages[current_client][i] = '\0';
			}
		}
		else if (mute_id[id][current_client] == 1)
		{
			for (i = 0; i < 50; i++)
			{
				sending_string[i] = '\0';
			}

			sprintf(sending_string, "[SERVER]: [%s] is ignoring you!\n\r", names[id]);

			send(client_socket[current_client], sending_string, (int)strlen(sending_string), 0);
		}
		else if (client_socket[id] == 0)
		{
			send(client_socket[current_client], "[SERVER]: User does not exist!\n\r", 33, 0);
		}
		else if (id == current_client)
		{
			send(client_socket[current_client], "[SERVER]: You can not pm to yourself!\n\r", 40, 0);
		}
	}
	else
	{
		send(client_socket[current_client], "[SERVER]: Wrong ID!\n\r", 22, 0);
	}
}

void command_mute(SOCKET client_socket[], char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE], int current_client, 
					int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS], char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH])
{
	char sending_string[MAX_NAME_LENGTH + 34];
	int i;
	int id = 0;
	char id_string[2] = { 0 };

	id_string[0] = messages[current_client][6];
	id_string[1] = messages[current_client][7];

	if (valid_id(id_string) == 0)
	{
		id = strtol(id_string, NULL, 10);

		if (client_socket[id] != 0 && id != current_client && mute_id[current_client][id] == 0) 
		{
			mute_id[current_client][id] = 1;
		
			for (i = 0; i < 50; i++) 
			{
				sending_string[i] = '\0';
			}
			
			sprintf(sending_string, "[SERVER]: [%s] has been muted!\r\n", names[id]);
			send(client_socket[current_client], sending_string, (int)strlen(sending_string), 0);
		}
		else if (id == current_client) 
		{
			send(client_socket[current_client], "[SERVER]: You can not mute yourself!\n\r", 39, 0);
		}
		else if (client_socket[id] == 0) 
		{
			send(client_socket[current_client], "[SERVER]: User does not exist!\n\r", 33, 0);
		}
		else if (mute_id[current_client][id] == 1) 
		{
			for (i = 0; i < 50; i++)
			{
				sending_string[i] = '\0';
			}
			
			sprintf(sending_string, "[SERVER]: [%s] is already muted!\r\n", names[id]);
			send(client_socket[current_client], sending_string, (int)strlen(sending_string), 0);
		}
	}
	else 
	{
		send(client_socket[current_client], "[SERVER]: Wrong ID!\n\r", 22, 0);
	}
}

void command_muteall (SOCKET client_socket[], int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS], int max_clients, int current_client)
{
	int i;

	for (i = 0; i < max_clients; i++)
	{
		mute_id[current_client][i] = 1;
	}

	send(client_socket[current_client], "[SERVER]: You have muted everyone!\r\n", 37, 0);
}

void command_unmute(SOCKET client_socket[], char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE], int current_client,
					int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS], char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH])
{
	char sending_string[MAX_NAME_LENGTH + 34];
	int i;
	int id = 0;
	char id_string[2] = { 0 };

	id_string[0] = messages[current_client][8];
	id_string[1] = messages[current_client][9];

	if (valid_id(id_string) == 0)
	{
		id = strtol(id_string, NULL, 10);

		if (client_socket[id] != 0 && id != current_client && mute_id[current_client][id] == 1)
		{
			mute_id[current_client][id] = 0;

			for (i = 0; i < 50; i++)
			{
				sending_string[i] = '\0';
			}

			sprintf(sending_string, "[SERVER]: [%s] has been unmuted!\r\n", names[id]);
			send(client_socket[current_client], sending_string, (int)strlen(sending_string), 0);
		}
		else if (id == current_client)
		{
			send(client_socket[current_client], "[SERVER]: You can not unmute yourself!\n\r", 41, 0);
		}
		else if (client_socket[id] == 0)
		{
			send(client_socket[current_client], "[SERVER]: User does not exist!\n\r", 33, 0);
		}
		else if (mute_id[current_client][id] == 0)
		{
			for (i = 0; i < 50; i++)
			{
				sending_string[i] = '\0';
			}

			sprintf(sending_string, "[SERVER]: [%s] is already unmuted!\r\n", names[id]);
			send(client_socket[current_client], sending_string, (int)strlen(sending_string), 0);
		}
	}
	else
	{
		send(client_socket[current_client], "[SERVER]: Wrong ID!\n\r", 22, 0);
	}
}

void command_unmuteall(SOCKET client_socket[], int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS], int max_clients, int current_client)
{
	int i;

	for (i = 0; i < max_clients; i++)
	{
		mute_id[current_client][i] = 0;
	}

	send(client_socket[current_client], "[SERVER]: You have unmuted everyone!\r\n", 39, 0);
}

void command_rename(SOCKET client_socket[], char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH], char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE],
					int current_client)
{
	char backed_up_name[MAX_NAME_LENGTH] = { 0 };
	char new_name[MAX_NAME_LENGTH] = { 0 };
	char sending_string[MAX_NAME_LENGTH + 34] = { 0 };
	int i;

	if (messages[current_client][8] == '\0')
	{
		send(client_socket[current_client], "[SERVER]: You can not have empty name!\r\n", 41, 0);
	}
	else if(messages[current_client][8] != '\0')
	{
		for (i = 0; i < MAX_NAME_LENGTH; i++)
		{
			if (messages[current_client][i + 8] != '\r')
			{
				new_name[i] = messages[current_client][i + 8];
			}
			else if (messages[current_client][i + 8] == '\r')
			{
				new_name[i] = '\0';
				break;
			}
		}

		for (i = 0; i < MAX_NAME_LENGTH; i++)
		{
			names[current_client][i] = '\0';
		}

		sprintf(names[current_client], "%s | ID: %d", new_name, current_client);

		for (i = 0; i < MAX_NAME_LENGTH + 34; i++)
		{
			sending_string[i] = '\0';
		}

		sprintf(sending_string, "[SERVER]: Your new nick is [%s]\r\n", names[current_client]);
		send(client_socket[current_client], sending_string, (int)strlen(sending_string), 0);
	}
}

void command_admin(SOCKET client_socket[], char password[], char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE], 
					 int admins[], int current_client)
{
	register int i;
	char password_to_test[MAX_PASSWORD_LENGTH];

	for (i = 0; i < strlen(messages[current_client]) - 7; i++)
	{
		password_to_test[i] = messages[current_client][i + 7];
	}

	password_to_test[i] = '\0';

	if (strlen(password) == strlen(password_to_test))
	{
		if (test_password(password_to_test, password) == 0)
		{
			if (admins[current_client] == 0)
			{
				admins[current_client] = 1;

				send(client_socket[current_client], "[SERVER]: You obtained admin privilages!\r\n", 43, 0);
			}
			else
			{
				send(client_socket[current_client], "[SERVER]: You have already obtained admin privilages!\r\n", 56, 0);
			}
		}
		else
		{
			send(client_socket[current_client], "[SERVER]: Wrong password!\r\n", 28, 0); // length fits
		}
	}
	else
	{
		send(client_socket[current_client], "[SERVER]: Wrong password!\r\n", 28, 0); // length is different
	}
}

void command_kick(SOCKET client_socket[], struct sockaddr_in address, FILE *fw, int admins[], int current_client, char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE], int max_clients,
					int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS], int letter_counter[], char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH])
{
	int i;
	int id = 0;
	char id_string[2] = { 0 };
	char client_kicked_msg[100] = { 0 };

	if (admins[current_client] == 1) 
	{
		id_string[0] = messages[current_client][6];
		id_string[1] = messages[current_client][7];

		if (valid_id(id_string) == 0)
		{
			id = strtol(id_string, NULL, 10);

			if (client_socket[id] != 0)
			{
				send(client_socket[id], "\r[SERVER]: You have been kicked by admin!", 41, 0);

				printf("\nClient disconnected!\n---------------------------------------");
				printf("\nSocket number:\t%d\nIP:\t\t%s\nPort\t\t%d\nIndex\t\t%d\n", (int)client_socket[current_client], inet_ntoa(address.sin_addr), ntohs(address.sin_port), current_client);
				printf("---------------------------------------\n");

				fprintf(fw, "\nClient disconnected!\n---------------------------------------");
				fprintf(fw, "\nSocket number:\t%d\nIP:\t\t%s\nPort\t\t%d\nIndex\t\t%d\n", (int)client_socket[current_client], inet_ntoa(address.sin_addr), ntohs(address.sin_port), current_client);
				fprintf(fw, "---------------------------------------\n");

				sprintf(client_kicked_msg, "[SERVER]: [%s] has been kicked by admin!\r\n", names[current_client]);

				closesocket(client_socket[id]);
				client_socket[id] = 0;

				for (i = 0; i < max_clients; i++)
				{
					mute_id[id][i] = 0;
				}

				for (i = 0; i < MAX_RECEIVE; i++)
				{
					messages[id][i] = '\0';
				}

				for (i = 0; i < MAX_NAME_LENGTH; i++)
				{
					names[id][i] = '\0';
				}

				letter_counter[id] = 0;
				admins[id] = 0;

				for (i = 0; i < max_clients; i++)
				{
					if (i != current_client && client_socket[i] != 0)
					{
						//move_message_after_server_message(client_kicked_msg, client_socket[i], messages[i], letter_counter[i]);
					}
				}
			}
			else 
			{
				send(client_socket[current_client], "[SERVER]: User does not exist!\r\n", 33, 0);
			}
		}
		else 
		{
			send(client_socket[current_client], "[SERVER]: Wrong ID!\r\n", 22, 0);
		}
	}
	else 
	{
		send(client_socket[current_client], "[SERVER]: You do not have admin privilages!\r\n", 46, 0);
	}
}

void command_shutdown(SOCKET client_socket[], WSADATA wsa, FILE *fw, int current_client, int admins[], int max_clients, int letter_counter[])
{
	register int i;

	if (admins[current_client] == 1)
	{
		for (i = 0; i < max_clients; i++)
		{
			if (client_socket != 0)
			{
				send(client_socket[i], "[SERVER]: Lobby was closed by admin!\r\n", 39, 0);	
			}
			closesocket(client_socket[i]);
		}

		WSACleanup();		
		fclose(fw);

		quit_function(); //dead end of program
	}
	else
	{
		send(client_socket[current_client], "[SERVER]: You do not have admin privilages!\r\n", 46, 0);
	}
}

void command_clearall(SOCKET client_socket[], char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE], int admins[], int current_client)
{
	register int i, j;
	char send_string[] = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n[SERVER]: Chat was fully cleared, including unfinished messages!\r\n";
	char send_string_2[] = "------------------------------------------------------------------------\r\n";

	if (admins[current_client] == 1)
	{
		for (i = 0; i < MAX_NUMBER_OF_USERS; i++)
		{
			for (j = 0; j < MAX_RECEIVE; j++)
			{
				messages[i][j] = '\0';
			}
		}

		for (i = 0; i < MAX_NUMBER_OF_USERS; i++)
		{
			send(client_socket[i], send_string_2, (int)strlen(send_string_2), 0);
			send(client_socket[i], send_string, (int)strlen(send_string), 0);
			send(client_socket[i], send_string_2, (int)strlen(send_string_2), 0);
		}
	}
	else
	{
		send(client_socket[current_client], "[SERVER]: You do not have admin privilages!\r\n", 46, 0);
	}
}

void command_users (SOCKET client_socket[], char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH], int max_clients, int active_client) 
{
	register int i;
	char sending_string[MAX_NAME_LENGTH + 4];

	for (i = 0; i < max_clients; i++) 
	{
		if (client_socket[i] != 0) 
		{
			sprintf(sending_string, "> %s\r\n", names[i]);
			send(client_socket[active_client], sending_string, (int)strlen(sending_string), 0);
		}
	}
}

void command_status (time_t start_watch, SOCKET client_socket[], int max_clients, int current_client, int admins[]) 
{
	int days = 0, hours = 0, minutes = 0, seconds = 0, remaining = 0;
	int timer = 0;
	char status_string[133] = { 0 };
	char time_string[50] = { 0 };

	timer = (int)((clock() - start_watch) / (double)CLOCKS_PER_SEC);

	days = timer / (24 * 3600);
	remaining = timer % (24 * 3600);
	hours = remaining / 3600;
	minutes = remaining / 60;
	seconds = remaining % 60;

	sprintf(time_string, "%dd %dh %dm %ds", days, hours, minutes, seconds);

	sprintf(status_string, "[SERVER]: Lobby uptime: %s | Online users: %d | Online admins: %d\r\n", time_string, online_users(client_socket, max_clients), online_admins(admins, max_clients));
	send(client_socket[current_client], status_string, (int)strlen(status_string), 0);
}

void command_help(SOCKET client_socket[], int current_client)
{
	char help_string_start[] = "\r\n===================== Commands =====================\r\n";
	char help_string_body[] = "help\r\n";
	char help_string_end[] = "====================================================\r\n\n";

	send(client_socket[current_client], help_string_start, (int)strlen(help_string_start), 0);
	send(client_socket[current_client], help_string_body, (int)strlen(help_string_body), 0);
	send(client_socket[current_client], help_string_end, (int)strlen(help_string_end), 0);
}