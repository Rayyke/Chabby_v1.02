/*
commands.h

Chabby 1.02 Beta
================

Latest changes done by Peter Cizmar (19.10.2016)
*/

#ifndef COMMANDS_H
#define COMMANDS_H

void command_pm(SOCKET client_socket[], 
	            int current_client, 
	            char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE],
				int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS], 
	            char current_client_name_string[],
				char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH]);

void command_mute(SOCKET client_socket[], 
					char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE], 
					int current_client,
					int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS], 
					char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH]);

void command_muteall(SOCKET client_socket[], 
					int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS], 
					int max_clients, 
					int current_client);

void command_unmute(SOCKET client_socket[], 
					char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE], 
					int current_client,
					int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS], 
					char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH]);

void command_unmuteall(SOCKET client_socket[], 
						int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS], 
						int max_clients, 
						int current_client);

void command_rename(SOCKET client_socket[], 
					char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH], 
					char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE],
					int current_client);

void command_admin(SOCKET client_socket[],
					char password[],
					char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE],
					int admins[],
					int current_client);

void command_kick(SOCKET client_socket[], 
					struct sockaddr_in address,
					FILE *fw,
					int admins[], 
					int current_client, 
					char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE], 
					int max_clients,
					int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS], 
					int letter_counter[], 
					char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH]);

void command_shutdown(SOCKET client_socket[],
						WSADATA wsa,
						FILE *fw,
						int current_client,
						int admins[],
						int max_clients,
						int letter_counter[]);

void command_clearall(SOCKET client_socket[], 
						char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE],
						int admins[], 
						int current_client);

void command_users(SOCKET client_socket[], 
					char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH], 
					int max_clients, 
					int current_client);

void command_status(time_t start_watch, 
					SOCKET client_socket[], 
					int max_clients, 
					int active_client, 
					int admins[]);

void command_help(SOCKET client_sockets[],
					int current_client);

#endif