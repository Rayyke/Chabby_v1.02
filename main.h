/*
main.h

Chabby 1.02 Beta
================

Latest changes done by Peter Cizmar (19.10.2016)
*/

#ifndef MAIN_H
#define MAIN_H

void move_message_after_user_message(int letter_counter, SOCKET client_socket, char message_for_receiver[], char message_of_current_client[],  char current_client_name_string[]);
void move_message_after_server_message(char server_msg[], SOCKET client_socket, char message_of_target_client[], int letter_counter);
void send_msg(SOCKET client_socket, char message_for_receiver[], char current_client_name_string[]);
void send_welcome_msg(SOCKET client_socket, char intro_message_string[]);
void quit_function();

#endif