/*
check_functions.h

Chabby 1.02 Beta
================

Latest changes done by Peter Cizmar (5.10.2016)
*/

#ifndef CHECK_FUNCTIONS_H
#define CHECK_FUNCTIONS_H

int test_password(char password_to_test[], char password[]);
int valid_id(char id_string[]);
int only_numbers(char string[]);
int online_users(SOCKET *client_socket, int max_clients);
int online_admins(int admins[], int max_clients);

#endif