/*
main.c

Chabby 1.02 Beta
================

Latest changes done by Peter Cizmar (19.10.2016)
*/

/*
> To connect to lobby, run console and type --> telnet xxx.xxx.xxx.xxx yyyy (example: telnet 192.168.0.1 8888)
> If you don�t have telnet allowed, run console and type --> pkgmgr /iu:"TelnetClient"
> Recommended size of client window is: [width 80+ | height: 50+]
*/

/* -----TODO------
> spam filter, via timers
> commands: -help
*/

#define _WINSOCK_DEPRECATED_NO_WARNINGS // use only for Visual Studio
#define _CRT_SECURE_NO_WARNINGS // use only for Visual Studio

#include "macros.h"
#include "libraries.h"
#include "main.h"
#include "check_functions.h"
#include "commands.h"

int main(int argc, char *argv[]) {
	WSADATA wsa; // premade structure used for initialising Winsock
	SOCKET master = 0, new_socket = 0, client_socket[MAX_NUMBER_OF_USERS + 1] = { 0 }; // sockets (master -> server, new -> temporary, sockets of users -> clients)
	fd_set readfds; // premade structure used for setting the different stuff
	FILE *fw; // file for writing all data displayed in console of running server
	time_t start_watch; // timer -> counting the server uptime

	register int i = 0;
	int current_client = 0;
	int port_number = 8888;
	int max_clients = MAX_NUMBER_OF_USERS;
	int activity = 0, addrlen = 0, valread = 0; // testing variables for sockets and receiving chars
	int letter_counter[MAX_NUMBER_OF_USERS] = { 0 }; // counting entered symbols by users before the msg is sent
	int admins[MAX_NUMBER_OF_USERS] = { 0 };
	int mute_id[MAX_NUMBER_OF_USERS][MAX_NUMBER_OF_USERS] = { 0 }; // who muted who?

	char port_number_string[33] = { 0 };
	char max_clients_string[33] = { 0 };
	char current_client_name_string[MAX_NAME_LENGTH] = { 0 }; // nick of client in form of "[User | ID: 0]"
	char client_connected_msg[100] = { 0 };
	char client_disconnected_msg[100] = { 0 };
	char password[100] = { 0 };
	char intro_message_string[33] = { 0 }; // intro string (displaying online users)
	char client_ip_string[33] = { 0 };
	char buffer[MAX_RECEIVE] = { 0 }; // array of chars entered by client
	char messages[MAX_NUMBER_OF_USERS][MAX_RECEIVE] = { 0 }; // gathering chars of certain users until "enter (CR)" char is received
	char names[MAX_NUMBER_OF_USERS][MAX_NAME_LENGTH] = { 0 }; // names of clients

	struct sockaddr_in server, address; // fucking complicated structures from Winsock.h library

	fw = fopen("server_log.txt", "w");

	// initialising Winsock
	printf("Initialising Winsock...\t\t\t\t\t");
	fprintf(fw, "Initialising Winsock...\t\t\t\t\t");

	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		ERR_MSG
	}
	else
	{
		OK_MSG
	}

	// creating master socket
	printf("Creating master socket...\t\t\t\t");
	fprintf(fw, "Creating master socket...\t\t\t\t");

	if ((master = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		ERR_MSG
	}	
	else
	{
		OK_MSG
	}
		
	printf("Enter port number (8888 recommended):\t\t\t");
	fprintf(fw, "Enter port number (8888 recommended):\t\t\t");
	scanf("%s", port_number_string);
	fprintf(fw, "%s\n", port_number_string);

	while (only_numbers(port_number_string) != 0) 
	{
		printf("\aEnter port number (8888 recommended):\t\t\t");
		fprintf(fw, "Enter port number (8888 recommended):\t\t\t");
		scanf("%s", port_number_string);
		fprintf(fw, "%s\n", port_number_string);
	}

	port_number = strtol(port_number_string, NULL, 10);

	// fulfilling structure,  concerning type of protocol (IPv4), IP, port
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(port_number);

	// binding master socket
	printf("Binding master socket...\t\t\t\t");
	fprintf(fw, "Binding master socket...\t\t\t\t");

	if (bind(master, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR)
	{
		ERR_MSG
	}
	else
	{
		OK_MSG
	}

	printf("Enter max clients (max %d):\t\t\t\t", MAX_NUMBER_OF_USERS);
	fprintf(fw, "Enter max clients (max %d):\t\t\t\t", MAX_NUMBER_OF_USERS);
	scanf("%s", max_clients_string);
	fprintf(fw, "%s\n", max_clients_string);

	max_clients = strtol(max_clients_string, NULL, 10);

	while (only_numbers(max_clients_string) != 0 || max_clients > MAX_NUMBER_OF_USERS || max_clients < 0) 
	{
		printf("\aEnter max clients (max %d):\t\t\t\t", MAX_NUMBER_OF_USERS);
		fprintf(fw, "Enter max clients (max %d):\t\t\t\t", MAX_NUMBER_OF_USERS);
		scanf("%s", max_clients_string);
		fprintf(fw, "%s\n", max_clients_string);

		max_clients = strtol(max_clients_string, NULL, 10);
	}

	printf("Enter admin password (no spaces, max length: %d):\t", MAX_PASSWORD_LENGTH);
	fprintf(fw, "Enter admin password (no spaces, max length: %d):\t", MAX_PASSWORD_LENGTH);
	scanf("%s", password);
	fprintf(fw, "%s\n", password);

	while (strlen(password) > MAX_PASSWORD_LENGTH)
	{
		printf("Password is too long!\n");
		fprintf(fw, "Password is too long!\n");
		printf("Enter admin password (no spaces, max length: %d):\t", MAX_PASSWORD_LENGTH);
		fprintf(fw, "Enter admin password (no spaces, max length: %d):\t", MAX_PASSWORD_LENGTH);
		scanf("%s", password);
		fprintf(fw, "%s\n", password);
	}

	// set master socket on listening mode
	listen(master, 3);

	printf("\nServer is ready to receive connections!\n");
	fprintf(fw, "\nServer is ready to receive connections!\n");

	addrlen = sizeof(struct sockaddr_in);

	// cleaning the array "who muted who"
	for (current_client = 0; i < MAX_NUMBER_OF_USERS; i++) 
	{
		for (i = 0; i < MAX_NUMBER_OF_USERS; i++) 
		{
			mute_id[current_client][i] = 0;
		}		
	}
	
	start_watch = clock();

	/***********************************************************************************************************************************************************/

	while (1)
	{
		FD_ZERO(&readfds); // clearing settings
		FD_SET(master, &readfds); // setting master socket

		// initialising/setting clients� sockets
		for (i = 0; i < max_clients; i++)
		{
			if (client_socket[i] > 0) 
			{
				FD_SET(client_socket[i], &readfds);
			}	
		}

		activity = select(0, &readfds, NULL, NULL, NULL); // waiting for activity on master socket

		if (activity == SOCKET_ERROR) 
		{
			ERR_MSG
		}
			
		// if something happens on master socket, someone client is trying to connect
		if (FD_ISSET(master, &readfds))
		{
			if ((new_socket = accept(master, (struct sockaddr *)&address, (int *)&addrlen)) < 0) 
			{
				ERR_MSG
			}
				
			// adding new client to the array of client sockets
			for (current_client = 0; current_client < max_clients + 1; current_client++) 
			{
				if (client_socket[current_client] == 0) 
				{
					if (current_client < max_clients) 
					{
						client_socket[current_client] = new_socket;

						sprintf(client_connected_msg, "[SERVER]: [User | ID: %d] has connected!\r\n", current_client);

						for (i = 0; i < max_clients; i++)
						{
							if (i != current_client && client_socket[i] != 0)
							{
								//move_message_after_server_message(client_connected_msg, client_socket[i], messages[i], letter_counter[i]);
							}	
						}

						sprintf(names[current_client], "User | ID: %d", current_client);

						break;
					}
					else 
					{
						client_socket[current_client] = new_socket;

						send(client_socket[current_client], ">>> Lobby is full <<<", 22, 0);

						closesocket(client_socket[current_client]);
						client_socket[current_client] = 0;

						break;
					}
				}		
			}

			// information displayed in the server console
			printf("\nNew client connected!\n---------------------------------------");
			printf("\nSocket number:\t%d\nIP:\t\t%s\nPort:\t\t%d\nIndex:\t\t%d\n", (int)new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port), current_client);
			printf("---------------------------------------\n");

			fprintf(fw, "\nNew client connected!\n---------------------------------------");
			fprintf(fw, "\nSocket number:\t%d\nIP:\t\t%s\nPort:\t\t%d\nIndex:\t\t%d\n", (int)new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port), current_client);
			fprintf(fw, "---------------------------------------\n");

			sprintf(intro_message_string, "(Online: %d)\n\r", online_users(client_socket, max_clients));
		
			send_welcome_msg(client_socket[current_client], intro_message_string);
		}

		// checking the activity of all client sockets
		for (current_client = 0; current_client < max_clients; current_client++)
		{   
			if (FD_ISSET(client_socket[current_client], &readfds))
			{
				getpeername(client_socket[current_client], (struct sockaddr*)&address, (int*)&addrlen);

				for (i = 0; i < (int)strlen(buffer); i++) // WAAAAAAt
				{
					buffer[i] = '\0';
				}

				valread = recv(client_socket[current_client], buffer, MAX_RECEIVE, 0); // gathering consequently all symbols that clients typed
				//strcat(buffer, "\0");

				if (valread == 0) // if there is no reaction from client�s socket...
				{
					// information displayed in the server console
					printf("\nClient disconnected!\n---------------------------------------");
					printf("\nSocket number:\t%d\nIP:\t\t%s\nPort\t\t%d\nIndex\t\t%d\n", (int)client_socket[current_client], inet_ntoa(address.sin_addr), ntohs(address.sin_port), current_client);
					printf("---------------------------------------\n");

					fprintf(fw, "\nClient disconnected!\n---------------------------------------");
					fprintf(fw, "\nSocket number:\t%d\nIP:\t\t%s\nPort\t\t%d\nIndex\t\t%d\n", (int)client_socket[current_client], inet_ntoa(address.sin_addr), ntohs(address.sin_port), current_client);
					fprintf(fw, "---------------------------------------\n");

					sprintf(client_disconnected_msg, "[SERVER]: [%s] has disconnected!\r\n", names[current_client]);

					// closing client, and freeing the space for otherclients
					closesocket(client_socket[current_client]);
					client_socket[current_client] = 0;

					for (i = 0; i < max_clients; i++)
					{
						mute_id[current_client][i] = 0;
					}

					for (i = 0; i < MAX_RECEIVE; i++)
					{
						messages[current_client][i] = '\0';
					}

					for (i = 0; i < MAX_NAME_LENGTH; i++) 
					{
						names[current_client][i] = '\0';
					}	

					letter_counter[current_client] = 0;
					admins[current_client] = 0;

					for (i = 0; i < max_clients; i++)
					{
						if (i != current_client && client_socket[i] != 0)
						{
							//move_message_after_server_message(client_disconnected_msg, client_socket[i], messages[i], letter_counter[i]);
						}
					}

					Sleep(10); // small delay after closing socket, because there was problem when more clients disconnected at the same time
				}
				else if(valread != 0)
				{
					for (i = 0; i < (int)strlen(buffer); i++)
					{
						if (buffer[i] == '\b' && letter_counter[current_client] > 0)
						{
							letter_counter[current_client]--;
						}
						else if (buffer[i] > 31 && buffer[i] != 127) // 127 -> DEL
						{
							if (letter_counter[current_client] >= MAX_RECEIVE)
							{
								send(client_socket[current_client], "\r\n[SERVER]: Your message is too long! Hit enter!\r\n", 51, 0);
								break; // don�t even finish loading from buffer...
							}
							else
							{
								messages[current_client][letter_counter[current_client]++] = buffer[i];
							}	
						}	
						else if (buffer[i] == '\r' && letter_counter[current_client] == 0)
						{
							send(client_socket[current_client], "[SERVER]: Empty messages are not allowed!\r\n", 44, 0);
						}
						else if (buffer[i] == '\r' && letter_counter[current_client] != 0)
						{
							messages[current_client][letter_counter[current_client]] = '\0';

							letter_counter[current_client] = 0;

							// vyrobim si citatelnu ip adresu aj portom vo formate --> "xxx.xxx.xxx.xxx:yyyy"
							sprintf(client_ip_string, "%s:%d - \0", inet_ntoa(address.sin_addr), ntohs(address.sin_port));
							sprintf(current_client_name_string, "[%s]: \0", names[current_client]);

							send(client_socket[current_client], "\r\n", 2, 0); // send 1 extra CR to sender
							
							printf("%s%s\r\n", client_ip_string, messages[current_client]); // kontrolny vypis do serveroveho terminalu
							fprintf(fw, "%s%s\r\n", client_ip_string, messages[current_client]); // kontrolny vypis do suboru

							// if the message starts with "-", it is command															 													
							if (messages[current_client][0] == '-')
							{
								// personal message
								if (strncmp(messages[current_client], "-pm ", 4) == 0)
								{
									command_pm(client_socket, current_client, messages, mute_id, current_client_name_string, names);
								}
								// mute annoying user, user won�t be able to see anything related to the muted target
								else if (strncmp(messages[current_client], "-mute ", 6) == 0) 
								{
									command_mute(client_socket, messages, current_client, mute_id, names);
								}
								// mute all incomming communication
								else if (strncmp(messages[current_client], "-muteall", 8) == 0) 
								{
									command_muteall(client_socket, mute_id, max_clients, current_client);
								}
								// unmute annoying user, user will be able to see everything related the unmuted target
								else if (strncmp(messages[current_client], "-unmute ", 8) == 0) 
								{
									command_unmute(client_socket, messages, current_client, mute_id, names);
								}
								// unmute all incomming communication
								else if (strncmp(messages[current_client], "-unmuteall", 10) == 0) 
								{
									command_unmuteall(client_socket, mute_id, max_clients, current_client);
								}
								// change the name of the user
								else if (strncmp(messages[current_client], "-rename ", 8) == 0) 
								{
									command_rename(client_socket, names, messages, current_client);
								}
								// log in to admin mode (allow: -kick, -amute, -aunmute)
								else if (strncmp(messages[current_client], "-admin ", 7) == 0) 
								{
									command_admin(client_socket, password, messages, admins, current_client);
								}
								// kick someone
								else if (strncmp(messages[current_client], "-kick ", 6) == 0) 
								{
									command_kick(client_socket, address, fw, admins, current_client, messages, max_clients, mute_id, letter_counter, names);
								}
								// shutdown server
								else if (strncmp(messages[current_client], "-shutdown", 9) == 0 && strlen(messages[current_client]) == 9)
								{
									command_shutdown(client_socket, wsa, fw, current_client, admins, max_clients, letter_counter);
								}
								// clear screen + messages array
								else if (strncmp(messages[current_client], "-clearall", 9) == 0 && strlen(messages[current_client]) == 9)
								{
									command_clearall(client_socket, messages, admins, current_client);
								}
								// info about server (uptime/online users & admins)
								else if (strncmp(messages[current_client], "-status", 7) == 0) 
								{ 
									command_status(start_watch, client_socket, max_clients, current_client, admins);
								}
								// list of connected users
								else if (strncmp(messages[current_client], "-users", 6) == 0) 
								{ 
									command_users(client_socket, names, max_clients, current_client);
								}
								else if (strncmp(messages[current_client], "-help", 5) == 0)
								{
									command_help(client_socket, current_client);
								}
								else {
									send(client_socket[current_client], "[SERVER]: Invalid command!\r\n", 29, 0);
								}
							}
							else 
							{
								// sending message to others
								for (i = 0; i < max_clients; i++) 
								{
									if (client_socket[i] != 0 && i != current_client && mute_id[i][current_client] == 0) 
									{
										if(letter_counter[current_client] != 0)
										{
											move_message_after_user_message(letter_counter[i], client_socket[i], messages[current_client], messages[i], current_client_name_string);
										}
										else
										{
											send_msg(client_socket[i], messages[current_client], current_client_name_string);
										}
									}
								}		
							}
						}
					}
				}
			}
		}
	}

	return 0;
}

void move_message_after_user_message(int letter_counter, SOCKET client_socket, char message_for_receiver[], char message_of_current_client[], char current_client_name_string[])
{
	int i;

	send(client_socket, "\r", 2, 0);

	for (i = 0; i < letter_counter; i++)
	{
		send(client_socket, " ", 2, 0);
	}

	send(client_socket, "\r", 2, 0);

	send_msg(client_socket, message_for_receiver, current_client_name_string);
	send(client_socket, message_of_current_client, (int)strlen(message_of_current_client), 0);
}

void move_message_after_server_message(char server_msg[], SOCKET client_socket, char message_of_target_client[], int letter_counter)
{
	int i;

	send(client_socket, "\r", 2, 0);

	for (i = 0; i < letter_counter; i++)
	{
		send(client_socket, " ", 2, 0);
	}

	send(client_socket, "\r", 2, 0);

	send(client_socket, server_msg, (int)strlen(server_msg), 0);
	send(client_socket, message_of_target_client, (int)strlen(message_of_target_client), 0);
}

void send_msg(SOCKET client_socket, char message_for_receiver[], char current_client_name_string[])
{
	send(client_socket, "\r", 1, 0);
	send(client_socket, current_client_name_string, (int)strlen(current_client_name_string), 0);
	send(client_socket, message_for_receiver, (int)strlen(message_for_receiver), 0);
	send(client_socket, "\r\n", 2, 0);
}

void send_welcome_msg(SOCKET client_socket, char intro_message_string[])
{
	char intro_message[] = "Welcome to Chabby 1.02 Beta!\n\n\r";
	char intro_message_2[] = "Connected to the chat lobby! ";
	char intro_message_3[] = "For list of commmands type -help and hit enter!\n\r";

	send(client_socket, intro_message, (int)strlen(intro_message), 0);
	send(client_socket, intro_message_2, (int)strlen(intro_message_2), 0);
	send(client_socket, intro_message_string, (int)strlen(intro_message_string), 0);
	send(client_socket, intro_message_3, (int)strlen(intro_message_3), 0);
	send(client_socket, "----------------------------------------------------\r\n", 55, 0);
}

void quit_function()
{
	exit(0);
}