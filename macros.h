/*
macros.h

Chabby 1.02 Beta
================

Latest changes done by Peter Cizmar (5.10.2016)
*/

#ifndef MACROS_H
#define MACROS_H

#define MAX_RECEIVE 1000 // max length of sent msg
#define MAX_NAME_LENGTH 32 // max length of name
#define MAX_PASSWORD_LENGTH 32
#define OK_MSG {printf("OK\n"); fprintf(fw, "OK\n");}
#define ERR_MSG {printf("\nFailed. Error (%d)\n\n", WSAGetLastError()); fprintf(fw, "\nFailed. Error (%d)\n\n", WSAGetLastError());}
#define MAX_NUMBER_OF_USERS 100

#endif